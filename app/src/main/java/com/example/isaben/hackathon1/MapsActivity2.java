package com.example.isaben.hackathon1;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

public class MapsActivity2 extends FragmentActivity implements OnMapReadyCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps2);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(getBaseContext(), MapsActivity.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng maceio = new LatLng(-9.5353394,-35.8270249);
        LatLng teresina = new LatLng(-5.1856982,-43.081674);
        LatLng joaopessoa = new LatLng(-7.1494914,-34.9534861);
        LatLng saoluis = new LatLng(-2.5604588,-44.3281626);
        LatLng natal = new LatLng(-5.7997439,-35.2922854);

        try{
            googleMap.setMyLocationEnabled(true);
        }
        catch(SecurityException se){
            System.err.println(se.getMessage());
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(maceio, 1));
        GoogleMapOptions options = new GoogleMapOptions();
        options.zoomControlsEnabled(true);


        googleMap.addPolyline(new PolylineOptions().geodesic(true).add(maceio).add(teresina).add(saoluis).add(natal).add(joaopessoa).add(maceio));

        googleMap.addMarker(new MarkerOptions().title("Maceio").snippet("Capital do estado com maior analfabetismo no Brasil").position(maceio));
        googleMap.addMarker(new MarkerOptions().title("Teresina").snippet("Capital do estado com segundo maior analfabetismo do Brasil").position(teresina));
        googleMap.addMarker(new MarkerOptions().title("Joao Pessoa").snippet("Capital do estado com terceiro maior analfabetismo do Brasil").position(joaopessoa));
        googleMap.addMarker(new MarkerOptions().title("Sao Luis").snippet("Capital do estado com quarto maior analfabetismo do Brasil").position(saoluis));
        googleMap.addMarker(new MarkerOptions().title("Natal").snippet("Capital do estado com quinto maior analfabetismo do Brasil").position(natal));

        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

    }
}
