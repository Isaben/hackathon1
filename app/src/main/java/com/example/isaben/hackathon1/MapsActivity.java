package com.example.isaben.hackathon1;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import android.support.design.widget.FloatingActionButton;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab1);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(getBaseContext(), MapsActivity2.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng caetano = new LatLng(-23.619269, -46.5603024);
        LatLng aguas = new LatLng(-22.6011653, -47.8927024);
        LatLng floripa = new LatLng(-27.6139544, -48.7630156);
        LatLng balneario = new LatLng(-27.0058574, -48.6870242);
        LatLng vitoria = new LatLng(-20.2821776, -40.3562874);



        try{
            googleMap.setMyLocationEnabled(true);
        }
        catch(SecurityException se){
            System.err.println(se.getMessage());
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(floripa, 1));

        googleMap.addPolyline(new PolylineOptions().geodesic(true).add(caetano).add(aguas).add(balneario).add(floripa).add(vitoria).add(caetano));

        googleMap.addMarker(new MarkerOptions().title("Caetano").snippet("Maior IDH do Brasil").position(caetano));
        googleMap.addMarker(new MarkerOptions().title("Águas").snippet("Segundo maior IDH do Brasil").position(aguas));
        googleMap.addMarker(new MarkerOptions().title("Floripa").snippet("Terceiro maior IDH do Brasil").position(floripa));
        googleMap.addMarker(new MarkerOptions().title("Balneario").snippet("Quarto maior IDH do Brasil").position(balneario));
        googleMap.addMarker(new MarkerOptions().title("Vitoria").snippet("Quinto maior IDH do Brasil").position(vitoria));

        googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

    }

}
